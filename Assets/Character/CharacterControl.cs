 	using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class CharacterControl : MonoBehaviour 
{
	private const int width = 32, height = 64;
	
	public Shield shield;
	public Rock m_rock;
	public GUITexture lives;
	
	#region Variables
	private bool move;
	private int hp;
	public GameObject wall;
	public Camera cam;
	private bool falling, canshield, endwalking;
	private float startTime, dTime, jumptime, bouncetime, cooldown;
	public int speed;
	private bool jumping, bouncing, dead;
	#endregion
	
	public bool CanMove
	{
		set { move = value; }
	}
	
	public bool CanShield
	{
		get { return canshield; }
		set { canshield = value; }
	}
	
	void Start () 
	{
		CanShield = true;
		hp = 10;
		lives.pixelInset = new Rect((hp * 100 * -1) / 2, 300, hp * 100, 58);
		CanMove = true;
	}
	
	
	public void KillMe()
	{
		transform.localScale = new Vector3(0,0,0);
		ParticleSystem[] dth = FindSceneObjectsOfType(typeof(ParticleSystem)) as ParticleSystem[];
		
		foreach(ParticleSystem ps in dth)
		{
			ps.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
			ps.Stop ();
			ps.Play();
		}
		
		startTime = 0;
		dead = true;
	}
	
	public void TakeDamage(int howmuch)
	{
		hp -= howmuch;
		
		lives.pixelInset = new Rect((hp * 100 * -1) / 2, 300, hp * 100, 58);
		
		if(hp == 0)
			KillMe ();
	}
	
	void Update () 
	{	
		if(dead) // dead
		{
			startTime += Time.deltaTime;
			
			if(startTime > 2.1f)
			{
				PlayerPrefs.SetInt ("Lives", PlayerPrefs.GetInt ("Lives") - 1);
				Application.LoadLevel("loader");
				
			}
			
			return;
		}
		
		if(!move)
		{
			return;
		}
		
		if(endwalking)
		{
			dTime += Time.deltaTime;
			Camera.mainCamera.transform.position = new Vector3(transform.position.x, 
																transform.position.y, 
																Camera.mainCamera.transform.position.z);
			
			rigidbody.velocity = Vector3.right * (speed / 2) + new Vector3(0, rigidbody.velocity.y, 0);
			
			if(dTime > 8f)
				Application.LoadLevel (PlayerPrefs.GetInt ("Level"));
			return;
		}
		
		if(bouncing)
		{
			bouncetime += Time.deltaTime;
			
			if (bouncetime > 1f) bouncing = false;
			return;
		}
		
		#region moving
		if(Input.GetAxis("Horizontal") != 0)
		{
			if(Input.GetAxis("Horizontal") > 0)
			{
				transform.localScale = (new Vector3(1, 1, 1));
				rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed + 
				                                     new Vector3(0, rigidbody.velocity.y, 0);						
			}
			else if(Input.GetAxis("Horizontal") < 0)
			{
				transform.localScale = (new Vector3(-1, 1, 1));
				rigidbody.velocity = Vector3.right * Input.GetAxis("Horizontal") * speed + 
				                                     new Vector3(0, rigidbody.velocity.y, 0);
			}
			
			if(Camera.mainCamera.WorldToViewportPoint(transform.position).x >= 1.001f)
			{
				cam.transform.position = new Vector3(cam.transform.position.x + 240, 1, -10);
				wall.transform.position = new Vector3(wall.transform.position.x + 240, wall.transform.position.y, -9);
				GameObject[] items = GameObject.FindGameObjectsWithTag("Finish");
				Vector3 temp = Camera.mainCamera.ViewportToWorldPoint(new Vector3(0.9f, 0.9f, 0));
				
				items[0].transform.position = Camera.mainCamera.ViewportToWorldPoint(new Vector3(0.1f, 0.9f, 1));
				items[1].transform.position = Camera.mainCamera.ViewportToWorldPoint(new Vector3(0.9f, 0.9f, 1));
			}
		}
		#endregion	
		
		
		#region input
		//: A
		if(Input.GetButtonDown("Fire1"))
		{
			if(CanShield)
			{
				Vector3 v = transform.position;
				v += (Vector3.right * 40 * transform.localScale.x);
				shield.UseShield (v, (int)transform.localScale.x);
			}
		}
		
		//: B
		if(Input.GetButtonDown("Fire2"))
		{
			GameObject rock = new GameObject();
			Rigidbody rb = rock.AddComponent(typeof(Rigidbody)) as Rigidbody;
			rb.useGravity = false;
			rb.freezeRotation = true;
			rb.mass = 404f;
			rock.layer = 8;
			rock.tag = "Respawn";
			
			Rock r = rock.AddComponent("Rock") as Rock;
			(rock.GetComponent ("BoxCollider") as BoxCollider).size = new Vector3(16,16,100);
			
			r.Throw(new Vector3(transform.position.x + (transform.localScale.x * width), 
				                                                transform.position.y, 0), 
				                                   (transform.localScale.x > 0) ? 1 : -1);
		}

		if(Input.GetButtonDown("Jump"))
		{
			if(!falling && !jumping)
			{
			  	jumping = true;
				jumptime = 0;
				transform.position += (Vector3.up * 10);
				return;
			}
		}
		
		if(Input.GetButtonUp("Jump"))
		{
			if(jumping)
			{
				rigidbody.velocity = Vector3.down * 0.2f;
				falling = true;	
				jumping = false;
				return;
			}
		}
		
		//if(Input.GetAxis("360_Triggers") > 0)
		{
			//:right trigger
		}
		
		//if(Input.GetAxis("360_Triggers") < 0)
		{
			//:left trigger
		}
		#endregion
		
		#region jumping
		if(jumping)
		{
			rigidbody.velocity = new Vector3(rigidbody.velocity.x, speed, 0);
			
			jumptime += Time.deltaTime;
			
			if(jumptime > 1.5f)
			{
				rigidbody.velocity = Vector3.down * 0.2f;
				falling = true;	
				jumping = false;
			}
		}
		#endregion
	}
	
	void OnCollisionEnter(Collision c)
	{
		if(c.gameObject.Equals(wall))
		{
			rigidbody.velocity = Vector3.right * speed;
			transform.position = new Vector3(transform.position.x + 8, transform.position.y, transform.position.z);
			bouncing = true;
			bouncetime = 0;
		}
		
		if(c.gameObject.GetComponent<Rock>() != null)
		{
			TakeDamage (2);
			GameObject.Destroy(c.gameObject);
			return;
		}
		
		if(c.gameObject.GetComponent<Spikes>() != null)
			KillMe();
		
		if(c.gameObject.GetComponent<FinishFlag>() != null)
		{
			dTime = 0;
			int level = PlayerPrefs.GetInt("Level");
			PlayerPrefs.SetInt ("Level", level + 1);
			endwalking = true;
			
			(c.gameObject.GetComponent<BoxCollider>()).size = Vector3.zero;
			(c.gameObject.GetComponent<BoxCollider>()).center = Vector3.one * 500;
			//switch(level) go next level
		}
		
		falling = false;
		jumping = false;
		
		//if(c.gameObject.Equals (rock.gameObject))
		//{
		//	rock.PickUp();
		//	audio.volume = 10f;
		//	audio.dopplerLevel = 1.5f;
		//	audio.Play();
		//}
	}
}
