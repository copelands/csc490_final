using UnityEngine;
using System.Collections;

using System.IO;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(Rigidbody))]
[ExecuteInEditMode]
public class Shield : MonoBehaviour
{	
	public CharacterControl cc;
	private Sprites spriteMgr;
	private Vector3[] verts;
	private Texture2D bg;
	private int _width, _height;
	private bool thrown;
	
	
	// Use this for initialization
	void Start () 
	{
		spriteMgr = new Sprites();
		
		bg = new Texture2D(35, 90);
		bg.LoadImage(File.ReadAllBytes(Directory.GetCurrentDirectory() + "/Art/shield.png"));
		
		_width = 32;
		_height = 64;
		UpdateMesh();
	}
	
	public void UseShield(Vector3 position, int direction)
	{
		if(thrown) return;
		
		
		transform.position = position;
		rigidbody.collider.transform.localScale = new Vector3(33, 68, 10);
		transform.localScale = new Vector3(direction, 1, 1);
		thrown = true;
	}
	
	void Update()
	{
		if(thrown)
		{
			rigidbody.velocity = new Vector3(200f * transform.localScale.x, 0, 0);
			
			Vector3 viewport = Camera.mainCamera.WorldToViewportPoint(transform.position);
			
			if(viewport.x > 1.1f || viewport.x < -0.1f)
			{
				thrown = false;
				cc.CanShield = true;
				rigidbody.velocity = Vector3.zero;
				transform.position += Vector3.down * 100;
			}
		}
	}
	
	void OnCollisionEnter(Collision c)
	{
	
	}
	
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{	
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("VertexLit"));
			
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = bg;
	}
}
