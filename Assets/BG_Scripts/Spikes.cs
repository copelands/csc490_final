using UnityEngine;
using System.Collections;

using System.IO;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(BoxCollider))]
[ExecuteInEditMode]
public class Spikes : MonoBehaviour
{		
	private Sprites spriteMgr;
	private Vector3[] verts;
	private Texture2D bg;
	private int _width, _height;
	
	
	// Use this for initialization
	void Start () 
	{
		spriteMgr = new Sprites();
		
		bg = new Texture2D(35, 90);
		bg.LoadImage(File.ReadAllBytes(Directory.GetCurrentDirectory() + "/Art/spikes.png"));
		
		_width = 32;
		_height = 64;
		UpdateMesh();
	}
	
	void Update()
	{

	}
	
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{	
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("VertexLit"));
			
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = bg;
	}
}
