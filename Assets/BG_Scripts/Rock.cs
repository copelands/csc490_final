using UnityEngine;
using System.Collections;

using System.IO;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(BoxCollider))]
[ExecuteInEditMode]
public class Rock : MonoBehaviour
{	
	public GameObject[] enemies;

	
	private Sprites spriteMgr;
	private Vector3[] verts;
	private Texture2D bg;
	private int _width, _height;
	private bool throwing, grounded;
	private float throwtime;
	private Vector3 viewport;
	private int direction;
	
	// Use this for initialization
	void Start () 
	{
		
		spriteMgr = new Sprites();
		
		bg = new Texture2D(16, 16);
		bg.LoadImage(File.ReadAllBytes(Directory.GetCurrentDirectory() + "/Art/rock.png"));
		
		_width = 16;
		_height = 16;
		UpdateMesh();
		
		//grounded = true;
	}
	
	public bool CanThrow()
	{
		return (!throwing && !grounded);
	}
	
	public void OnCollisionEnter(Collision cc)
	{
		if(cc.gameObject.GetComponent<Shield>() != null)
		{
			GameObject.Destroy (this.gameObject);
		}
	}
	
	public void ResetItem()
	{
		Vector3 temp = Camera.mainCamera.ViewportToWorldPoint(new Vector3(0.9f, 0.9f, 0));
		transform.position = new Vector3(temp.x, temp.y, transform.position.z);
		rigidbody.useGravity = false;
		rigidbody.velocity = Vector3.zero;
	}
	
	public void Throw(Vector3 pos, int way)
	{
		transform.position = pos;
		throwing = true;
		throwtime = 0;
		rigidbody.useGravity = true;
		
		direction = way;
	}
	
	void Update()
	{
		if(throwing)
		{
			if(throwtime == 0)
				rigidbody.velocity = new Vector3(200f * direction, 5, 0);
			
			throwtime += Time.deltaTime;
			
			viewport = Camera.mainCamera.WorldToViewportPoint(transform.position);
			
			if(viewport.x > 1f || viewport.x < 0f)
			{
				if(transform.localScale.x == 1)
					GameObject.Destroy(this.gameObject);
				throwing = false;
				grounded = false;
				ResetItem ();
			}
		}
	}
	
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{	
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("Unlit/Transparent"));
			
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = bg;
	}
}
