using UnityEngine;
using System.Collections;

public class Cursor : MonoBehaviour 
{
	
	public GUITexture csr;
	public Title title;
	private float[] yvalues = new float[] {-88f, -120f};
	private float xvalue, width, height;
	private float dtime;
	private int which;
	
	void Start () 
	{
		xvalue = csr.pixelInset.x;
		width = csr.pixelInset.width;
		height = csr.pixelInset.height;
		
		dtime = 0;
		which = 0;
	}
	
	void Update () 
	{
		dtime += Time.deltaTime;
		
		if(dtime >= 0.5)
		{
			if(Input.GetAxis ("Vertical") != 0)
			{
				dtime = 0;
				csr.pixelInset = new Rect(xvalue, yvalues[which++], width, height);
				
				title.Change(which);

				if(which == 2) which = 0;
			}
			
			if(csr.pixelInset.y == yvalues[0])
			{
				if(Input.GetButtonDown("Fire1") || Input.GetKey(KeyCode.Return))
				{
					Application.LoadLevel ("loader");
				}
			}
		}
	}
}
