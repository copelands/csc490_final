using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class Probe : MonoBehaviour 
{
	public Character c;
	
	// Use this for initialization
	void Start () 
	{
	
	}
	
	void OnCollisionEnter(Collision cc)
	{
		if(cc.gameObject.GetComponent<CharacterControl>() != null)
		{
			(cc.gameObject.GetComponent<CharacterControl>()).TakeDamage(5);
		}
		
		GameObject.Destroy (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Camera.mainCamera.WorldToViewportPoint(transform.position).x < 0)
			GameObject.Destroy (this.gameObject);
		
		
		if((c.transform.position.x - transform.position.x) > -10)
		{
			rigidbody.velocity = (Vector3.up + Vector3.left) * 60;
		}
		
		GameObject[] rocks = GameObject.FindGameObjectsWithTag("Respawn");

		if(rocks != null)
			foreach(GameObject go in rocks)
			{
				if(go.layer != 8) continue;
			
				if((go.transform.position.x - transform.position.x) > -10)
				{
					rigidbody.velocity = (Vector3.up + Vector3.left) * 60;
				}
			}
	}
}
