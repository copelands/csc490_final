using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour 
{
	public GUIText txt;
	private float dtime;
	
	// Use this for initialization
	void Start () 
	{
		dtime = 0;
		txt.text = "Lives: " + PlayerPrefs.GetInt ("Lives");
		
		if(PlayerPrefs.GetInt ("Lives") == 0)
		{
			PlayerPrefs.SetInt ("Level", 0);
			PlayerPrefs.SetInt ("Lives", 3);
			txt.text = "Game Over";
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		dtime += Time.deltaTime;
		
		if(dtime >= 2f)
			Application.LoadLevel(PlayerPrefs.GetInt ("Level"));
	}
}
