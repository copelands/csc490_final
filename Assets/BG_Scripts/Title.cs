using UnityEngine;
using System.Collections;
using System.IO;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class Title : MonoBehaviour 
{
	public const int titl = 1, ctlr = 2;
	
	private Sprites spriteMgr;
	private Vector3[] verts;
	private Texture2D bg;
	private int _width, _height;
	
	
	// Use this for initialization
	void Start () 
	{
		spriteMgr = new Sprites();
		
		bg = new Texture2D(887, 200);
		bg.LoadImage(File.ReadAllBytes(Directory.GetCurrentDirectory() + spriteMgr.title));
		
		_width = bg.width;
		_height = bg.height;
		UpdateMesh();
		PlayerPrefs.SetInt ("Level", 1);
		PlayerPrefs.SetInt ("Lives", 3);
	}
	
	void Update()
	{

	}
	
	public void Change(int which)
	{
		GUIText[] text = GameObject.FindObjectsOfType(typeof(GUIText)) as GUIText[];
		
		switch(which)
		{
			case 1:
				bg = new Texture2D(887, 200);
				bg.LoadImage(File.ReadAllBytes(Directory.GetCurrentDirectory() + spriteMgr.title));
				transform.position = new Vector3(transform.position.x, 68.5f, 0);
				foreach(GUIText g in text)
					g.material.color = Color.white;
			
				break;
			
			case 2:
				bg = new Texture2D(887, 500);
				bg.LoadImage(File.ReadAllBytes(Directory.GetCurrentDirectory() + spriteMgr.controls));
				transform.position = new Vector3(transform.position.x, 20f, 0);
				foreach(GUIText g in text)
					g.material.color = Color.black;
			
				break;
		}
		
		
		_height = bg.height;
		UpdateMesh ();
	}
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{	
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("VertexLit"));
			
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
		material.mainTexture = bg;
	}
}
