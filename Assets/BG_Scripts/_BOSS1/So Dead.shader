Shader "Custom/So Dead" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "black" {}
		_Color ("Main Color", Color) = (0,0,0,1)
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		lighting on
		
		Pass
		{
			Material
			{
				Diffuse[_Color]
			}
		}
	} 
	FallBack "Diffuse"
}
