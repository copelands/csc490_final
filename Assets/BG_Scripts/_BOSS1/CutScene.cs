using UnityEngine;
using System.Collections;


//========================================
//: TODO:
//:	  Make switch(answer) more efficient
//========================================


public class CutScene : MonoBehaviour 
{
	public CharacterControl cc;
	public GameObject cube;
	
	public four four;
	public twentytwo tt;
	public fish fsh;
	public lol wat;
	
	public GameObject go;
	public GUIText txt;
	
	private float dtime;
	private int part = 1;
	private int answer;
	private bool sodead = false;
	private bool played = false;
	
	// Use this for initialization
	void Start () 
	{
		System.Random rr = new System.Random();
		
		answer = rr.Next (3);
		cube.renderer.material.color = new Color(0, 0, 0.1f, 255);
	}
	
	// Update is called once per frame
	void Update () 
	{
		dtime += Time.deltaTime;
		
		if(dtime >= 4f)
		{
			cube.renderer.material.color = new Color(Mathf.Min (255, cube.renderer.material.color.r + 0.1f), 0, 0.1f, 255);
			
			if(cube.renderer.material.color.r >= 0.9f && !played)
			{
				audio.Play();
				played = true;
			}
			
			if(cube.renderer.material.color.r >= 1.5f && part != -5)
				part = -6;
			
			switch(part)
			{
				case -6:
					txt.text = "WHAT DID I TELL YOU ABOUT THE RED?";
					PlayerPrefs.SetInt ("Lives", PlayerPrefs.GetInt ("Lives") - 1);
					cc.CanMove = false;
					break;
				
				case -5:
					Application.LoadLevel ("loader");
					break;
				
				case -4:
					sodead = true;
					txt.text = "You win this round...\r\nBUT I WILL BE BACK\r\npossibly with weapons.....";
					break;
				
				case -3:
					Application.LoadLevel("finish");
				//:replace with loadlevel 3 later
					break;
				
				case -1:
					txt.text = "WRONG ANSWER FOOL!";
					sodead = true;
					break;
				
				case 0:
					Application.LoadLevel ("loader");
					break;
				
				case 1:
					cc.CanMove = false;
					txt.text = "I want to play a game\r\nwith you...";
					break;
				
				case 2:
					txt.text = "It's simple, just choose\r\nthe\"correct\" answer...";
					break;
				
				case 3: 
					txt.text = "Don't let the red get you...\r\nBEGIN!";
					break;
				
				case 4:
					txt.text = "What is 2+2?";
					cc.CanMove = true;
					go.transform.position = new Vector3(10, 0, 0);
					break;
			}
			part++;
			dtime = 0;
		}
		
		
		if(sodead) return;
		
		switch(answer)
		{
			case 0:
				if(four.answer || tt.answer || wat.answer)
				{
					PlayerPrefs.SetInt ("Lives", PlayerPrefs.GetInt ("Lives") - 1);
					PlayerPrefs.SetInt ("Level", 2);
					part = -1;
					sodead = true;
				}
				
				if(fsh.answer)
				{
					PlayerPrefs.SetInt ("Level", 3);
					part = -4;
				}
				break;
				
			case 1:
				if(fsh.answer || tt.answer || wat.answer)
				{
					PlayerPrefs.SetInt ("Lives", PlayerPrefs.GetInt ("Lives") - 1);
					PlayerPrefs.SetInt ("Level", 2);
					sodead = true;
					part = -1;
				}
				
				if(four.answer)
				{
					PlayerPrefs.SetInt ("Level", 3);
					part = -4;
				}
				break;
				
			case 2:
				if(four.answer || fsh.answer || wat.answer)
				{
					PlayerPrefs.SetInt ("Lives", PlayerPrefs.GetInt ("Lives") - 1);
					PlayerPrefs.SetInt ("Level", 2);
					sodead = true;
					part = -1;
				}
				
				if(tt.answer)
				{
					PlayerPrefs.SetInt ("Level", 3);
					part = -4;
				}
				break;
				
			case 3: 
				if(four.answer || tt.answer || fsh.answer)
				{
					PlayerPrefs.SetInt ("Lives", PlayerPrefs.GetInt ("Lives") - 1);
					PlayerPrefs.SetInt ("Level", 2);
					sodead = true;
					part = -1;
				}
				
				if(wat.answer)
				{
					PlayerPrefs.SetInt ("Level", 3);
					part = -4;
				}
				break;
		}
	}
}
