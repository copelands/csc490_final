using UnityEngine;
using System.Collections;
using System.IO;
using System;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(BoxCollider))]
[ExecuteInEditMode]
public class RectangleBro : MonoBehaviour
{	
	public GameObject wall;
	public CharacterControl mainguy;
	private Sprites spriteMgr;
	private Vector3[] verts;
	private Texture2D[] bg;
	private int _width, _height;
	private float xx, yy;
	private bool good;
	private float timetillchange;
	
	
	// Use this for initialization
	void Start () 
	{
		spriteMgr = new Sprites();
		
		bg = new Texture2D[]
				{
					new Texture2D(100,30), 
					new Texture2D(100,30)
				};
		
		bg[0].LoadImage(File.ReadAllBytes(Directory.GetCurrentDirectory() + "/Art/RBB.png"));
		bg[1].LoadImage(File.ReadAllBytes(Directory.GetCurrentDirectory() + "/Art/RBG.png"));
		
		_width = 32;
		_height = 64;
		timetillchange = 2;
		UpdateMesh();
		
		xx = ((BoxCollider)collider).size.x;
		yy = ((BoxCollider)collider).size.y;
	}
	
	void Update()
	{
		if(wall.transform.position.x > transform.position.x)
			Destroy (this);
		
		
		timetillchange -= Time.deltaTime;
		
		if(timetillchange <= 0)
		{
			good = !good;
			
			if(good)
			{
				timetillchange = 5;
				((BoxCollider)collider).size = new Vector3(xx, yy, 85);
			}
			else
			{
				timetillchange = 10;
				((BoxCollider)collider).size = new Vector3(xx, yy, 0);	
				mainguy.rigidbody.AddForce(Vector3.down);
			}
			
			UpdateMesh ();
		}
	}
	
	
	[ContextMenu("UpdateMesh")]
	void UpdateMesh()
	{	
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if(!mesh)
		{
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		
		Material material = GetComponent<MeshRenderer>().sharedMaterial;
		if(!material)
		{
			material = new Material(Shader.Find ("VertexLit"));
			
			GetComponent<MeshRenderer>().sharedMaterial = material;
		}
				
		verts = new Vector3[]
		{
			new Vector3( _width / 2.0f,  _height / 2.0f, 0),
			new Vector3( _width / 2.0f, -_height / 2.0f, 0),
			new Vector3(-_width / 2.0f,  _height / 2.0f, 0),
			new Vector3(-_width / 2.0f, -_height / 2.0f, 0)
		};
		
		mesh.vertices = verts;
		mesh.uv = spriteMgr.UVs;
		mesh.triangles = spriteMgr.Triangles;
		mesh.normals = spriteMgr.Norms;
	
		material.mainTexture = (good) ? bg[1] : bg[0];
	}
}
